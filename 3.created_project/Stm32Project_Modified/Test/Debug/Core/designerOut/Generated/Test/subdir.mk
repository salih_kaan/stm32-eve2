################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/designerOut/Generated/Test/App__Generated.c \
../Core/designerOut/Generated/Test/MainPage__Generated.c 

OBJS += \
./Core/designerOut/Generated/Test/App__Generated.o \
./Core/designerOut/Generated/Test/MainPage__Generated.o 

C_DEPS += \
./Core/designerOut/Generated/Test/App__Generated.d \
./Core/designerOut/Generated/Test/MainPage__Generated.d 


# Each subdirectory must supply rules for building sources it contributes
Core/designerOut/Generated/Test/%.o: ../Core/designerOut/Generated/Test/%.c Core/designerOut/Generated/Test/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F429xx -DDISPLAY_RESOLUTION_QVGA -DEVE_DISPLAY_AVAILABLE -DEVE_GRAPHICS_AVAILABLE -DFT813_ENABLE -DNHD_3_5C_FT813 -DStm32_PLATFORM -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

