################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/designerOut/Libraries/FT_Eve_Hal/FT_CoPro_Cmds.c \
../Core/designerOut/Libraries/FT_Eve_Hal/FT_Emu_main.c \
../Core/designerOut/Libraries/FT_Eve_Hal/FT_Gpu_Hal.c \
../Core/designerOut/Libraries/FT_Eve_Hal/FT_ILI9488.c 

OBJS += \
./Core/designerOut/Libraries/FT_Eve_Hal/FT_CoPro_Cmds.o \
./Core/designerOut/Libraries/FT_Eve_Hal/FT_Emu_main.o \
./Core/designerOut/Libraries/FT_Eve_Hal/FT_Gpu_Hal.o \
./Core/designerOut/Libraries/FT_Eve_Hal/FT_ILI9488.o 

C_DEPS += \
./Core/designerOut/Libraries/FT_Eve_Hal/FT_CoPro_Cmds.d \
./Core/designerOut/Libraries/FT_Eve_Hal/FT_Emu_main.d \
./Core/designerOut/Libraries/FT_Eve_Hal/FT_Gpu_Hal.d \
./Core/designerOut/Libraries/FT_Eve_Hal/FT_ILI9488.d 


# Each subdirectory must supply rules for building sources it contributes
Core/designerOut/Libraries/FT_Eve_Hal/%.o: ../Core/designerOut/Libraries/FT_Eve_Hal/%.c Core/designerOut/Libraries/FT_Eve_Hal/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F429xx -DDISPLAY_RESOLUTION_QVGA -DEVE_DISPLAY_AVAILABLE -DEVE_GRAPHICS_AVAILABLE -DFT813_ENABLE -DNHD_3_5C_FT813 -DStm32_PLATFORM -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

