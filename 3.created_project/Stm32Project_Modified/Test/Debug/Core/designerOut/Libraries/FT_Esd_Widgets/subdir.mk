################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_CheckBox__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Clock__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ColorPicker.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ColorPicker__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_DemoPage__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Elements.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Elements__h__Simulation.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Gauge__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ImageButton__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_IntCounter__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_LabelButton__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Label__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_NumericLabel__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_PosSizeToRadius__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ProgressBar__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_PushButton__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_RadioButton.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_RadioButton__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_RadioGroup.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_RadioGroup__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Rectangle__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ScrollPanel.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ScrollPanel__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Slider.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Slider__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_SpinBox__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Theme.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Theme_DarkOrange.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Theme_LightBlue.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Theme__h__Simulation.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Toggle__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/LayoutMargin__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/LayoutSplit__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/SwitchBool__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Widgets/SwitchPage.c 

OBJS += \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_CheckBox__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Clock__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ColorPicker.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ColorPicker__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_DemoPage__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Elements.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Elements__h__Simulation.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Gauge__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ImageButton__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_IntCounter__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_LabelButton__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Label__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_NumericLabel__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_PosSizeToRadius__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ProgressBar__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_PushButton__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_RadioButton.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_RadioButton__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_RadioGroup.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_RadioGroup__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Rectangle__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ScrollPanel.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ScrollPanel__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Slider.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Slider__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_SpinBox__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Theme.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Theme_DarkOrange.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Theme_LightBlue.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Theme__h__Simulation.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Toggle__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/LayoutMargin__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/LayoutSplit__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/SwitchBool__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Widgets/SwitchPage.o 

C_DEPS += \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_CheckBox__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Clock__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ColorPicker.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ColorPicker__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_DemoPage__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Elements.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Elements__h__Simulation.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Gauge__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ImageButton__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_IntCounter__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_LabelButton__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Label__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_NumericLabel__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_PosSizeToRadius__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ProgressBar__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_PushButton__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_RadioButton.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_RadioButton__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_RadioGroup.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_RadioGroup__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Rectangle__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ScrollPanel.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_ScrollPanel__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Slider.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Slider__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_SpinBox__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Theme.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Theme_DarkOrange.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Theme_LightBlue.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Theme__h__Simulation.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/Ft_Esd_Toggle__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/LayoutMargin__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/LayoutSplit__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/SwitchBool__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Widgets/SwitchPage.d 


# Each subdirectory must supply rules for building sources it contributes
Core/designerOut/Libraries/FT_Esd_Widgets/%.o: ../Core/designerOut/Libraries/FT_Esd_Widgets/%.c Core/designerOut/Libraries/FT_Esd_Widgets/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F429xx -DDISPLAY_RESOLUTION_QVGA -DEVE_DISPLAY_AVAILABLE -DEVE_GRAPHICS_AVAILABLE -DFT813_ENABLE -DNHD_3_5C_FT813 -DStm32_PLATFORM -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

