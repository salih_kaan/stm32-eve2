################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/designerOut/Libraries/FT_Esd_Framework/FT_Esd_CoPro_Cmds__h__Simulation.c \
../Core/designerOut/Libraries/FT_Esd_Framework/FT_Esd_Dl.c \
../Core/designerOut/Libraries/FT_Esd_Framework/FT_Esd_MainLoop.c \
../Core/designerOut/Libraries/FT_Esd_Framework/FT_Esd_Primitives.c \
../Core/designerOut/Libraries/FT_Esd_Framework/FT_Esd_Primitives__h__Simulation.c \
../Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd.c \
../Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_App.c \
../Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_BitmapInfo.c \
../Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_GpuAlloc.c \
../Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_Timer.c \
../Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_Timer__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_TouchArea.c \
../Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_TouchArea__Generated.c \
../Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_TouchTag.c 

OBJS += \
./Core/designerOut/Libraries/FT_Esd_Framework/FT_Esd_CoPro_Cmds__h__Simulation.o \
./Core/designerOut/Libraries/FT_Esd_Framework/FT_Esd_Dl.o \
./Core/designerOut/Libraries/FT_Esd_Framework/FT_Esd_MainLoop.o \
./Core/designerOut/Libraries/FT_Esd_Framework/FT_Esd_Primitives.o \
./Core/designerOut/Libraries/FT_Esd_Framework/FT_Esd_Primitives__h__Simulation.o \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd.o \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_App.o \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_BitmapInfo.o \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_GpuAlloc.o \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_Timer.o \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_Timer__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_TouchArea.o \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_TouchArea__Generated.o \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_TouchTag.o 

C_DEPS += \
./Core/designerOut/Libraries/FT_Esd_Framework/FT_Esd_CoPro_Cmds__h__Simulation.d \
./Core/designerOut/Libraries/FT_Esd_Framework/FT_Esd_Dl.d \
./Core/designerOut/Libraries/FT_Esd_Framework/FT_Esd_MainLoop.d \
./Core/designerOut/Libraries/FT_Esd_Framework/FT_Esd_Primitives.d \
./Core/designerOut/Libraries/FT_Esd_Framework/FT_Esd_Primitives__h__Simulation.d \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd.d \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_App.d \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_BitmapInfo.d \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_GpuAlloc.d \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_Timer.d \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_Timer__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_TouchArea.d \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_TouchArea__Generated.d \
./Core/designerOut/Libraries/FT_Esd_Framework/Ft_Esd_TouchTag.d 


# Each subdirectory must supply rules for building sources it contributes
Core/designerOut/Libraries/FT_Esd_Framework/%.o: ../Core/designerOut/Libraries/FT_Esd_Framework/%.c Core/designerOut/Libraries/FT_Esd_Framework/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F429xx -DDISPLAY_RESOLUTION_QVGA -DEVE_DISPLAY_AVAILABLE -DEVE_GRAPHICS_AVAILABLE -DFT813_ENABLE -DNHD_3_5C_FT813 -DStm32_PLATFORM -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

